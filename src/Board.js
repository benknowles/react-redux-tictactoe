import React from 'react';
import Square from './Square';
import { connect } from 'react-redux';
import { move } from './state/actions';

class Board extends React.Component {
    renderSquare(i) {
        return <Square value={this.props.squares[i]} onClick={() => this.props.handleClick(i)} />;
    }
    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        squares: state.squares,
        turn: state.turn,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleClick: (i) => {
            dispatch(move(i));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);