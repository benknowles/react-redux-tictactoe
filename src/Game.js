import React from 'react';
import Board from './Board';
import { connect } from 'react-redux';
import { reset } from './state/actions';
import './Game.css';
        
let ResetButton = (props) => {
    if (!props.winner)
        return null;
    return <button className="reset-button" onClick={props.onClick}>Reset</button>;
};

class Game extends React.Component {
    render() {
        const winner = this.props.winner;
        let status;

        if (winner) {
            status = 'Winner: ' + winner;
        } else if (winner === 0) {
            status = 'Draw!';
        } else {
            status = 'Next player: ' + (this.props.turn ? 'O' : 'X');
        }
        
        return (
            <div className="game">
                <div className="game-board">
                    <div className="status">{status}</div>
                    <Board />
                </div>
                <div className="game-info">
                    Turn: {this.props.turn ? 'O' : 'X'}
                    <div>
                        <ResetButton winner={winner} onClick={this.props.resetGame} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        turn: state.turn,
        winner: state.winner,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetGame: () => {
            dispatch(reset());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Game);