import { MOVE, RESET } from '../state/actions';
import * as Rules from '../Rules';

const initialState = {
    squares: Array(9).fill(null),
    turn: true,
    winner: null,
};

export default function tictactoe(state = initialState, action) {
    switch (action.type) {
        case MOVE:
            const squares = state.squares.slice();

            if (state.winner || squares[action.index])
                return state;
                
            squares[action.index] = state.turn ? 'o' : 'x';

            return {
                squares: squares,
                turn: !state.turn,
                winner: Rules.winner(squares),
            };
        case RESET:
            return initialState;
        default:
            return state;
    }
}