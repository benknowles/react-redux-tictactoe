export const MOVE = 'MOVE';
export const RESET = 'RESET';

export function move(index) {
    return {
        type: MOVE,
        index,
    }
}

export function reset() {
    return {
        type: RESET,
    }
}